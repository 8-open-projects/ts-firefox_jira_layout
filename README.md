# Jira Kanbanboard layout changer

![under dev](<https://img.shields.io/badge/Experimental-Under%20Development%20%3B)-%231dbbd5>)

> This small firefox extension will change some css-style to have a better overview over the issues.</br></br>
> The new version from Jira, changed the kanbanboard to a wide-range view, where you do no longer have a good overview over the issues.</br>

## Will change following

### 01. Some CSS-style to

```css
ul.ghx-columns,
ul.ghx-column-headers {
  display: table !important;
  border-spacing: 2px 0 !important;
}

.ghx-column-headers li.ghx-column {
  padding-top: 1px !important;
  padding: 3px 3px !important;
}

div.ghx-detail-view {
  display: none !important;
}

div.aui-sidebar {
  display: none !important;
}

.ghx-rapid-views #gh #ghx-work #ghx-pool-column .ghx-swimlane .ghx-swimlane-header {
  top: 30px;
}
```

### 02. Make the Link on the Issues direct clickable as new Tab

```ts
...
document.querySelectorAll("a.ghx-key-link");
...
window.open(element.getAttribute("href"), "_blank");
...
```
