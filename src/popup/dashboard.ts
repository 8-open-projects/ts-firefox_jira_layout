/**
 * CSS to hide everything on the page,
 * except for elements that have the "beastify-image" class.
 */
const hidePage = `body > :not(.beastify-image) {
  opacity: 1;
}`;

interface ModeMessage {
  mode: ModeTypes;
  status: boolean;
}

enum ModeTypes {
  JIRA_CONTENT = "JIRA_CONTENT",
  RESET = "RESET"
}

interface ModeStatus {
  jira: {
    active: boolean;
  };
}

const modulesStatus: ModeStatus = {
  jira: { active: false }
};

/*######################################################################################### */
/*######################################################################################### */
/*######################################################################################### */

/**
 * Listen for clicks on the buttons, and send the appropriate message to
 * the content script in the page.
 */
function listenForClicks() {
  document.addEventListener("click", e => {
    const target: EventTarget = e.target;
    if (target instanceof HTMLInputElement) {
      const targetElement: HTMLInputElement = target;

      // get relevant values
      const changeTo: boolean = targetElement.checked;

      // This not works, thats why the switch down is implemented
      let modeType: ModeTypes = ModeTypes.JIRA_CONTENT[targetElement.id];

      if (modeType == null)
        switch (targetElement.id) {
          case ModeTypes.JIRA_CONTENT:
            modeType = ModeTypes.JIRA_CONTENT;
            break;
          case ModeTypes.RESET:
            modeType = ModeTypes.RESET;
            break;
        }

      /**
       * Insert the page-hiding CSS into the active tab,
       * then get the beast URL and
       * send a "beastify" message to the content script in the active tab.
       *
       * @param tabs
       * @param todo
       */
      function runOnModeSwitch(tabs, todo: { changeTo: boolean; modeType: ModeTypes }) {
        browser.tabs
          .insertCSS({
            code: hidePage
          })
          .then(() => {
            try {
              const modeMessage: ModeMessage = {
                mode: todo.modeType,
                status: modulesStatus.jira.active = todo.changeTo
              };

              if (modeMessage.mode != null && modeMessage.status != null) browser.tabs.sendMessage(tabs[0].id, modeMessage);
            } catch (e) {
              console.error(`[E] Failed in (runOnModeSwitch) ${e}`);
            }
          });
      }

      // Just log the error to the console.
      function reportError(error) {
        console.error(`Could not beastify: ${error}`);
      }

      /*######################################################################################### */
      /*######################################################################################### */
      /*######################################################################################### */

      // Get the active tab, and call the function
      browser.tabs
        .query({
          active: true,
          currentWindow: true
        })
        .then(tabs => {
          runOnModeSwitch(tabs, { changeTo: changeTo, modeType: modeType });
        })
        .catch(reportError);
    }
  });
}

/**
 * There was an error executing the script.
 * Display the popup's error message, and hide the normal UI.
 */
function reportExecuteScriptError(error) {
  document.querySelector("#popup-content").classList.add("hidden");
  document.querySelector("#error-content").classList.remove("hidden");
  console.error(`Failed to execute beastify content script: ${error.message}`);
}

/*######################################################################################### */
/*######################################################################################### */
/*######################################################################################### */

/**
 * When the popup loads, inject a content script into the active tab,
 * and add a click handler.
 * If we couldn't inject the script, handle the error.
 */
browser.tabs
  .executeScript({
    file: "/content_scripts/layout_changer.js"
  })
  .then(listenForClicks)
  .catch(reportExecuteScriptError);

/*######################################################################################### */
/*######################################################################################### */
/*######################################################################################### */
