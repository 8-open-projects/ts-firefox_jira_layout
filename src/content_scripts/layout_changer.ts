interface ModeMessage {
  mode: ModeTypes;
  status: boolean;
}

enum ModeTypes {
  JIRA_CONTENT = "JIRA_CONTENT",
  RESET = "RESET"
}

(function() {
  /**
   * Check and set a global guard variable.
   * If this content script is injected into the same page again,
   * it will do nothing next time.
   */
  // const win: Window & typeof globalThis = window;
  // if (window["hasRun"] != null && window["hasRun"] === true) return;
  // window["hasRun"] = true;

  /**
   * Listen for messages from the background script.
   * Call "beastify()" or "reset()".
   */
  browser.runtime.onMessage.addListener(msg => {
    try {
      const message: ModeMessage = msg;
      if (message.mode === ModeTypes.JIRA_CONTENT) {
        changeJiraLayout();
      } else if (message.mode === ModeTypes.RESET) {
        browser.tabs.reload().then(
          msg => {
            console.debug(msg);
          },
          err => {
            console.debug(err);
          }
        );
      }
    } catch (e) {
      console.error(`[E] Failed in (onMessage) ${e}`);
    }
  });
})();

async function changeJiraLayout(): Promise<void> {
  try {
    // document.body.style.border = "5px solid red";

    document.styleSheets;

    // collection of for to look
    const listOfElements: { selector: string; type: "div" | "link" }[] = [
      { selector: "ul.ghx-columns", type: "div" },
      { selector: "ul.ghx-column-headers", type: "div" },
      { selector: "a.ghx-key-link", type: "link" }
    ];

    addJiraLayoutCss();

    // loop over selection and change defined style
    for (let elem of listOfElements) {
      // get all selectors from html
      const nodeElem: NodeListOf<HTMLElement> = document.querySelectorAll(elem.selector);

      // run over all for current founded selection and change the style
      for (const element of nodeElem) {
        // if is a "div", change it that way
        // if (elem.type === "div") {
        //   element.style.display = "table";
        //   element.style.borderSpacing = "2px 0";
        // }
        // if it is a "link", add this one
        // else
        if (elem.type === "link") {
          // element.setAttribute("target", "_blank");

          element.addEventListener("click", () => {
            // const elementsDetail: NodeListOf<HTMLElement> = document.querySelectorAll("div.ghx-detail-view");
            // for (const element of elementsDetail) element.style.display = "none";
            window.open(element.getAttribute("href"), "_blank");
          });
        }
      }
    }
  } catch (e) {
    console.error(`[E] Failed in (changeJiraLayout) ${e}`);
  }
}

function addJiraLayoutCss(): HTMLStyleElement {
  try {
    let style = document.createElement("style");
    style.innerHTML = `
  ul.ghx-columns,
  ul.ghx-column-headers {
    display: table !important;
    border-spacing: 2px 0 !important;
  }
  .ghx-column-headers li.ghx-column{
    padding-top: 1px  !important;
    padding: 3px 3px  !important;
  }
  div.ghx-detail-view {
    display: none !important;
  }
  div.aui-sidebar {
    display: none !important;
  }
  .ghx-rapid-views #gh #ghx-work #ghx-pool-column .ghx-swimlane .ghx-swimlane-header{
    top: 30px;
  }
  `;
    document.head.appendChild(style);
    return style;
  } catch (e) {
    console.error(`[E] Failed in (addJiraLayoutCss) ${e}`);
  }
  return null;
}
